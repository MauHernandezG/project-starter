# Indicius' Awesome Boilerplate

## Installing and starting the boilerplate
To start working with our Awesome Boilerplate, we need to install some dependencies. To do this, run this command:

```
$ npm run start
```

Other available commands:

```
$ npm run code // Starts and serves the boilerplate
$ npm run build // Builds the project
```

## File Structure
Our file structure is the following:

```
├── gulp/
└── src/
   ├── assets/
   |  ├── fonts/
   |  ├── head/
   |  |  ├── favicon.png
   |  |  └── manifest.json
   |  ├── icons/
   |  |  └── react.svg
   |  ├── images/
   |  ├── js/
   |  |  ├── app.js
   |  |  └── index.js
   |  └── styles/
   |     ├── main/
   |     |  ├── _mixins.scss
   |     |  ├── _variables.scss
   |     |  ├── components/
   |     |  |  └── _menu.scss
   |     |  ├── main.scss
   |     |  └── screens/
   |     |     └── _base.scss
   |     └── vendor/
   |        ├── _reset.scss
   |        └── vendor.scss
   └── markup/
      ├── components/
      |  ├── footer.pug
      |  └── menu.pug
      ├── layouts/
      |  ├── includes/
      |  |  └── mixins.pug
      |  └── layout-primary.pug
      └── screens/
         ├── index.pug
├── gulpfile.js
├── package-lock.json
├── package.json

```

## Markup
We use [`PUG`](https://pugjs.org/api/getting-started.html) as our HTML postprocessor.
Inside the `src/markup/` folder you'll find the following directories:

* **`components/`**: your general components —such as a `footer` or `menu`— are meant to live here. Please feel free to add any other component that should be available to any screen.
* **`layouts/`**: here is where your `layout-primary.pug` lives. There're cases where you need more than one layout per website, so feel free to add a new one if you are required to. 
  * **`includes/`: your [`pug mixins`](https://pugjs.org/language/mixins.html) are stored here. Mixins are an amazing tool that saves you a lot of time while developing. Use it!
* **`screens/`**: we encourage you to code-split each of your screen, so it's more easy to understand its structure and to maintain it. 

## Styling
We use [`SASS`](https://sass-lang.com/) as our CSS postprocessor.<br/>
All our project files are imported in the `main.scss` file. Import your vendor stylesheets inside the `vendor.scss` file.
General components —such as a `container` or a `menu`— stylesheets are inside the `src/assets/styles/main/components/`. These components are meant to be available to any screen.<br/>
Screen-specific stylesheets are meant to live inside the `src/assets/styles/main/screens/` directory.

## Scripting
All your scripts must be inside the `src/assets/js/` directory.

## Fonts
All your font files must be inside the `src/assets/fonts` directory.

## Head
You HEAD files, such as your **favicon** files, and your **manifest.json** must be stored here. 

## Icons
You can place your SVG's in the `src/assets/icons/` directory and access them inside the markup using our mixin `+svg()`.

## Images
Insert your images in the `src/assets/images/` directory. All these images will be compressed and optimized once you build the project.

## Deploying
To deploy your project just push your files into the GitLab repository that we provided to you. Each of the branches of your project will be automatically deployed with an unique URL, following the `https://#{project-name}-git-#{branch-name}.indicius.now.sh` naming convention. For the `master` branch, the URL will be: `https://#{project-name}.indicius.com`.<br/>To know more about how you should handle the branches of your project, you can check [`our developer guides`](https://developer-guidelines.indicius.now.sh/).
