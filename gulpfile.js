const config = require('./gulp/config.js')
const gulp = require('gulp')
const prod = config.production

// Common
const common = [
  'fonts',
  'markup',
  'scripts',
  'static',
  'styles'
]

// Development
const development = [
  'serve',
  'watch'
]

// Production
const production = [
  'minifyStyles',
  'optimizeImages'
]

// Requires files
common.forEach(file => require(`./gulp/common/${ file }`))
prod ? 
  production.forEach(file => require(`./gulp/production/${ file }`))
:
  development.forEach(file => require(`./gulp/development/${ file }`))

let tasks = [
  'clean',
  'styles',
  'images',
  'head',
  'scripts',
  'fonts',
  'markup',
  prod && [...production]
]

const filteredTasks = tasks.filter(task => !!task)

// GULP Tasks
gulp.task('build', gulp.series(...filteredTasks))

if (!prod) {
  gulp.task('serve', gulp.parallel('browser-sync', 'watch'))
  gulp.task('default', gulp.series('build', 'serve'))
}