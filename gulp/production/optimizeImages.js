const gulp = require('gulp')
const config = require('../config')
const images = require('gulp-image')

gulp.task('optimizeImages', () => 
  gulp.src( config.directories.public.images + '**/*' )
    .pipe(images())
    .pipe(gulp.dest(config.directories.public.images))
)