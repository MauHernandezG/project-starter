const gulp = require('gulp')
const config = require('../config')
const rename = require('gulp-rename')
const cleanCSS = require('gulp-clean-css')

gulp.task('minifyStyles', () => 
  gulp.src( config.directories.public.styles + '/*.css' )
    .pipe( rename({
      suffix: '.min'
    }))
    .pipe( cleanCSS() )
    .pipe( gulp.dest( config.directories.public.styles ))
)
